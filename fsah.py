#!/usr/bin/env python

import argparse
import os
import requests
import time
import subprocess
import sys

AUR_URL = "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h="
VER_URL = "https://raw.githubusercontent.com/sxnvte/FSAH/main/ver"

session = requests.Session()
responsee = requests.get(VER_URL)
raw_content = responsee.text

ver = "1.0.3"

if os.geteuid()==0:
    print("Please don`t run FSAH as root!")
    sys.exit()

username = os.getlogin()
fsahdatadir = f"/home/{username}/FSAH-data"

if not os.path.isdir(f"/home/{username}/FSAH-data"):
    os.makedirs(f"/home/{username}/FSAH-data")

def download_package(package_name):
    aur_package_url = AUR_URL + package_name
    response = session.get(aur_package_url)
    if response.status_code == 200:
        package_content = response.content.decode()
        package_folder = os.path.join(fsahdatadir, package_name + "-aur")
        if not os.path.isdir(package_folder):
            os.makedirs(package_folder)
        with open(os.path.join(package_folder, "PKGBUILD"), "w") as f:
            f.write(package_content)
        subprocess.run(["makepkg", "-si"], cwd=package_folder)
    else:
        print("FSAH - package not found on AUR.")

def remove_package(package_name):
    package_folder = os.path.join(fsahdatadir, package_name + "-aur")
    if os.path.isdir(package_folder):
        subprocess.run(["sudo", "pacman", "-R", package_name], check=True)
        os.system("sudo pacman -R " + package_name)
        os.chdir("..")
        os.system("rm -rf " + package_folder)
    else:
        print("FSAH - package not found")

def search_package(package_name):
    aur_package_url = AUR_URL + package_name
    response = session.get(aur_package_url)
    if response.status_code == 200:
        print(f"FSAH - package that contains name '{package_name}' was found on AUR.")
    else:
        print(f"FSAH - package that contains name '{package_name}' was not found on AUR.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="FSAH - Fucking Simple AUR Helper by sxnvte")
    parser.add_argument("-S", "--download", action="store_true", help="download the package")
    parser.add_argument("-R", "--remove", action="store_true", help="remove the package")
    parser.add_argument("-Ss" ,"--search", action="store_true", help="check if package exist on AUR using name")
    parser.add_argument("-v", "--version", action="store_true", help="displays version")
    parser.add_argument("package_name", type=str, help="name of the package to download", nargs='?')

    args = parser.parse_args()

    if args.download and args.package_name:
        download_package(args.package_name)
    elif args.remove and args.package_name:
        remove_package(args.package_name)
    elif args.search and args.package_name:
        search_package(args.package_name)
    elif args.version:
        print(f"FSAH {ver}")
        sys.stdout.flush()
    elif not args.download:
        print("unknown usage To FSAH was detected! to see usage use command 'fsah --help'")
        sys.stdout.flush()
    else:
        print("please specify the name of the package!")
        sys.stdout.flush()
