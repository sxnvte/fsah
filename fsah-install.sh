#!/bin/bash

# fun fact: this installation script was made by chatgpt
echo "Welcome to FSAH installer"

SCRIPT_NAME="fsah.py"


SCRIPT_PATH=$(realpath $SCRIPT_NAME)

if [ ! -f "$SCRIPT_PATH" ]; then
    echo "cannot find $SCRIPT_NAME"
    exit 1
fi


INSTALL_PATH="/usr/bin/fsah"


sudo cp "$SCRIPT_PATH" "$INSTALL_PATH"


sudo chmod +x "$INSTALL_PATH"

echo "FSAH has been installed in $INSTALL_PATH"
