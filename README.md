<h1 align="center">FSAH</h1>
<p align="center">Fucking Simple AUR Helper</p>
</br>

<h1 align="center">About</h1>
FSAH - Fucking Simple AUR Helper is a AUR helper written in Python. I was kinda bored that I thought to write AUR helper.
</br>
 

<h1 align="center">Installation</h1>

- Clone repository

```shell
$ git clone https://gitlab.com/sxnvte/fsah.git
```

- cd into it

```shell
$ cd fsah
```

- add executing permissions to installation script

```shell
$ chmod +x fsah-install.sh
```

- start the script and you are done!

```shell
$ ./fsah-install.sh
```
</br>

btw you dont even need to install it! you just can use this command

```shell
$ python fsah.py
```

but when installed you can access it from terminal shortly

```shell
$ fsah
```
## If you want to uninstall

```shell
$ chmod +x fsah-uninstall.sh
```
then
```shell
$ ./fsah-uninstall.sh
```

<h1 align="center">Usage</h1>
To download package use --download flag

```shell
$ fsah-dev --download firefox
```
or if you want to use "pacman style flags"

```shell
$ fsah-dev -S firefox
```

To remove package use --remove flag

```shell
$ fsah --remove firefox
```

or if you want to use "pacman style flags"

```shell
$ fsah-dev -R firefox
```

its that easy!

